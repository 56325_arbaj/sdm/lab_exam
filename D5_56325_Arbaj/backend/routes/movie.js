const express = require('express')
const db = require('../db')
const utils = require('..utils')

const router = express.Router()

router.post('/add', (request, response) => {
    const { movie_id, movie_title, movie_release_date, movie_time, director_name } = request.body

  const connection = db.openConnection()

  const statement = `
  insert into Movie
    (movie_id, movie_title, movie_release_date, movie_time, director_name)
  values
    ('${movie_id}', '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(utils.createResult(error, result))
  })

})

router.get('/', (request, response) => {
    const { movie_title } = request.params

  const connection = db.openConnection()

  const statement = `
  select * from movie where movie_title = ${movie_title}
    (movie_id, movie_title, movie_release_date, movie_time, director_name)
  values
    ('${movie_id}', '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(utils.createResult(error, result))
  })

})

router.delete('/:id', (request, response) => {
        const { movie_id } = request.params
      
        const statement = `
          delete from movie
          where
             movie_id = ${movie_id}
        `
      
        const connection = db.openConnection()
        connection.query(statement, (error, result) => {
          connection.end()
          response.send(utils.createResult(error, result))
        })
      })

router.put('/:id', (request, response) => {
    const { movie_release_date, movie_time } = request.body
    const { movie_id } = request.params
        
    const statement = `
        update movie
        set 
        movie_release_date = '${movie_release_date}', 
        movie_time = '${movie_time}'
                
        where
        movie_id = ${movie_id}
        `
        
        const connection = db.openConnection()
        connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
        })
    })








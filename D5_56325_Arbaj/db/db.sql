create table Movie (
    movie_id integer primary key auto_increment, 
    movie_title VARCHAR(50), 
    movie_release_date date,
    movie_time VARCHAR(50),
    director_name VARCHAR(50)
)